<?php

namespace AkamuraAsai\Example\Controllers;

use AkamuraAsai\Example\Models\Example;
use AkamuraAsai\Example\Repositories\ExampleRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExampleController extends Controller
{
    protected $repository;

    public function __construct(ExampleRepository $repo)
    {
        $this->repository = $repo;
    }

    public function index()
    {
        return view('example::index');
    }

    public function all()
    {
        return $this->repository->findAll();
    }

    public function post(Request $request)
    {
        $example = $request->all();
        if (isset($example['codigo']) && $example['codigo'] != ''){
            $this->salvar($example);
        } else {
            $this->inserir($example);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->all();
        $registro = $this->repository->find($id['codigo']);
        return $this->repository->delete($registro);
    }

    private function salvar($example)
    {
        $registro = $this->repository->find($example['codigo']);
        $registro->nome = $example['nome'];
        return $this->repository->save($registro);
    }

    private function inserir($example)
    {
        $registro = new Example();
        $registro->nome = $example['nome'];
        return $this->repository->save($registro);
    }

}