<?php

Route::group(['prefix' => 'example'], function () {
    Route::get('/', 'AkamuraAsai\Example\Controllers\ExampleController@index');
    Route::get('/all', 'AkamuraAsai\Example\Controllers\ExampleController@all');
    Route::post('/', 'AkamuraAsai\Example\Controllers\ExampleController@post');
    Route::post('/delete', 'AkamuraAsai\Example\Controllers\ExampleController@delete');
});