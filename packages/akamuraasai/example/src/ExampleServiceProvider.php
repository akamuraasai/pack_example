<?php

namespace AkamuraAsai\Example;

use Illuminate\Support\ServiceProvider;

class ExampleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/Views', 'example');
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
        $this->publishes([
            __DIR__.'/Views' => base_path('resources/views/akamuraasai/example'),
        ], 'views');
        $this->publishes([
            __DIR__.'/Assets' => public_path('akamuraasai/example'),
        ], 'public_assets');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/Routes/routes.php';
        $this->app->make('AkamuraAsai\Example\Controllers\ExampleController');

    }
}
