<?php

namespace AkamuraAsai\Example\Models;

use Illuminate\Database\Eloquent\Model;

class Example extends Model
{
    protected $fillable = [
        'nome',
    ];
}
