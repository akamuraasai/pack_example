<?php
namespace AkamuraAsai\Example\Repositories;

use AkamuraAsai\Example\Models\Example;

class ExampleRepository
{
    public function findAll()
    {
        return Example::all();
    }

    public function find($id)
    {
        return Example::find($id);
    }

    public function save(Example $example)
    {
        $example->save();
    }

    public function delete(Example $example)
    {
        $example->delete();
    }
}