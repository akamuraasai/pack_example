<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Laravel 5.x Package Example</title>
    <link rel="stylesheet" type="text/css" href="/akamuraasai/example/semantic.min.css">

    <style type="text/css">
        ::-webkit-scrollbar {
            width: 4px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            -webkit-border-radius: 10px;
            border-radius: 10px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            -webkit-border-radius: 10px;
            border-radius: 10px;
            background: rgba(255,255,255,0.8);
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.5);
        }
        ::-webkit-scrollbar-thumb:window-inactive {
            background: rgba(255,255,255,0.4);
        }
    </style>
    @yield('css-especifico')

    <script src="/akamuraasai/example/jquery.min.js"></script>
    <script src="/akamuraasai/example/semantic.min.js"></script>
    <script src="/akamuraasai/example/angular.min.js"></script>
</head>
<body>

@yield('content')

</body>
</html>