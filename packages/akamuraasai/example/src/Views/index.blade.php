@extends('example::layouts.main')

@section(('css-especifico'))
    <style>
        .landpage-image{
            background-color: #fcfcff !important;
            margin: auto 10px;
            width: auto;
            height: 2000px;
        }
    </style>
@stop

@section('content')
    <div class="ui vertical masthead aligned segment landpage-image" ng-app="exampleApp" ng-controller="exampleCtrl" ng-init="listar();">

        <div class="ui container">
            <div class="ui large secondary pointing menu">
                <a class="active item">Exemplo de uso de package no Laravel 5.x</a>
            </div>
        </div><br>

        <div class="ui container">
            <h1 class="ui header"></h1>
            <h4 class="ui dividing header">
                Lista de Exemplos
            </h4>

            <table class="ui table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th class="center aligned">Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in itens">
                        <td class="collapsing">@{{ item.id }}</td>
                        <td>@{{ item.nome }}</td>
                        <td class="collapsing right aligned">
                            <button class="tiny ui inverted yellow button" ng-click="editar( item )">Editar</button>
                            <button class="tiny ui inverted red button" ng-click="excluir( item.id )">Excluir</button>
                        </td>
                    </tr>

                    <tr ng-show="!itens.length">
                        <td colspan="3" class="warning center aligned" style="font-size: medium;">Nenhum registro encontrado no banco de dados.</td>
                    </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="3" class="right aligned">
                        <button class="tiny ui blue button" ng-click="novo()">Novo Registro</button>
                    </th>
                </tr>
                </tfoot>
            </table>
        </div><br><br><br>

        <div class="ui container">
            <h1 class="ui header"></h1>
            <h4 class="ui dividing header">Informações do Exemplo</h4>

            <form class="ui form" role="form" method="POST" action="{{ url('/example') }}">
                {!! csrf_field() !!}
                <input type="text" hidden name="codigo" id="codigo" ng-model="item.id">
                <div class="field">
                    <label>Nome do Exemplo</label>
                    <div class="ui fluid corner labeled left icon input" data-content="É necessário digitar um nome para que o registro seja salvo." data-variation="wide inverted" required>
                        <i class="circular inverted browser icon"></i>
                        <input type="text" name="nome" id="nome" placeholder="Ex.: exemplo de nome" ng-model="item.nome">
                        <div class="ui corner label">
                            <i class="asterisk icon"></i>
                        </div>
                    </div>
                </div>
            </form><br><br>
            <div class="ui grid">
                <div class="six column row">
                    <div class="right floated column">
                        <button type="button" class="tiny ui button" ng-click="novo()">Cancelar</button>
                        <button type="button" class="tiny ui green button" ng-click="salvar()">Salvar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="application/javascript">
        var app = angular.module('exampleApp', []);
        app.controller('exampleCtrl', function($scope, $http) {
            $scope.item = {};
            $scope.itens = [];

            $scope.listar = function () {
                $http.get("/example/all")
                        .then(function(resposta) {
                            $scope.itens = resposta.data;
                        });
            }

            $scope.novo = function () {
                $scope.item = {};
                $('#nome').focus();
            }

            $scope.editar = function (item) {
                $scope.item = item;
                $('#nome').focus();
            }

            $scope.validaCampos = function (item) {
                var retorno = true;

                if (item.nome === undefined || item.nome.length < 6) {
                    retorno = false;
                    $('#nome').parent().parent().addClass("error");
                } else {
                    $('#nome').parent().parent().removeClass("error");
                }

                return retorno;
            }

            $scope.salvar = function () {
                var item = $scope.item;
                if (!$scope.validaCampos(item)) return;
                $http.post("/example",
                        {
                            codigo: item.id,
                            nome: item.nome
                        }
                ).then(function(resposta) {
                    $scope.listar();
                    $scope.novo();
                });
            }

            $scope.excluir = function (id) {
                if (confirm("Tem certeza que deseja excluir o Exemplo?")) {
                    $http.post("/example/delete",
                            {
                                codigo: id
                            }
                    ).then(function(resposta) {
                        $scope.listar();
                    });
                }
            }
        });
    </script>
@stop