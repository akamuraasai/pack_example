# Laravel 5.x Packages Example

## Como utilizar

Para utilizar este repositório, basta seguir estes passos:

1. Clonar o repositorio
2. Configurar o arquivo .env
3. Rodar o comando "composer install" para instalar os pacotes necessários
4. Rodar o comando "php artisan vendor:publish" para publicar os Assets
5. Rodar o comando "php artisan migrate" para gerar as tabelas necessárias
6. Rodar o comando "php artisan serve" para iniciar a aplicação
7. Enjoy ;)

## Licença de Uso

MIT.